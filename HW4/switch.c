#include <stdlib.h>
#include <stdio.h>
#include <dirent.h>
#include <errno.h>
#include <sys/stat.h>
#include <string.h>
#include <ctype.h>
#include <unistd.h>

struct userArgs;
//User input and misc 
struct userArgs{
	char* searchStr;
	char* replaceStr;
	char* fname;
	int mode;
};
//Globals
const int LENGTH = 1024;

//Prototypes
void searchDirectories(const char *directory, const char* context, const char* path, struct userArgs* userArg);
char* parseStrings(char* parseStr);
char* replacer(char* theStr, char* find, char* replace);
void mode1(char* myFile_path, struct userArgs* userArg);

//Replaces text with new text
//param:
//1: The string passed in
//2: The substring to find
//3: The substring to replace 2 with
char* replacer(char* theStr, char* find, char* replace)
{
	char* rv;
	char* tmp;
	char* inString;
	int length_rest;
	int length_find;
	int length_replace;
	int i;

	length_find = (int)strlen(find);
	length_replace = (int)strlen(replace);

	inString = strstr(theStr, find);
	if(inString == NULL)
		return theStr;
	
	//get count of characters that are going to be replaced
	for(i=0; (tmp=strstr(inString, find)); ++i)
	{
		inString = tmp+length_find;
		//printf("tmp %s inString %s", tmp, inString);
	}
	//mallocs space for the string plus size of string it is replaced with, which could be negative
	tmp = rv = (char*)malloc(strlen(theStr)+(length_replace-length_find)*i+1);
	//replaces string one at a time
	while(i--)
	{
		inString = strstr(theStr, find);
		length_rest = (int)(inString-theStr);
		tmp = strncpy(tmp, theStr, length_rest) + length_rest;
		tmp = strcpy(tmp, replace) + length_replace;
		theStr += length_rest + length_find;
	}
	strcpy(tmp, theStr);
	return rv;
}


//Searches for either Directory file renaming or file text renaming
void searchDirectories(const char *directory, const char* context, const char* path, struct userArgs* userArg)
{
	//Parent directory 
	if(path)
	{
		if(strncmp(path, "..", strlen(path)) == 0 || strncmp(path, ".", strlen(path))==0)
		{
			return;
		}
	}
	//printf("here?");
	DIR* myDir;
	struct dirent* meta;
	struct stat* curr_file_status;
	char* search_path = (char*)malloc(sizeof(char) * LENGTH);
	char* rel_path = (char*)malloc(sizeof(char) * LENGTH);

	if(path)
	{
		if(context){
			snprintf(search_path, LENGTH, "%s/%s/%s", myDir, context, path);
			snprintf(rel_path, LENGTH, "%s/%s", context, path);
		} else {
			snprintf(search_path, LENGTH, "%s/%s", myDir, path);
			strncpy(rel_path, path, LENGTH);
		}
	}
	else
	{
		rel_path = NULL;
		strncpy(search_path, directory, strlen(myDir)+1);
		printf("searchPath = %s\n", search_path);
	}
	printf("Search Path: %s \n", search_path);
	//opens the file and cancels program if directory does not exist
	//Program freezes here
	if(!(myDir = opendir(directory))){
		printf("anything");
		fprintf(stderr, "No directory", search_path);
		return;
	}
	else
	{
		//printf("reach here?");
	curr_file_status = (struct stat*)malloc(sizeof(struct stat));
	while((meta = readdir(myDir)))
	{
		//printf("infinite loop here?");
		if(meta->d_type == DT_DIR)
		{
			searchDirectories(myDir, path, meta->d_name, userArg);
			if((userArg->fname != NULL && strstr(search_path, userArg->fname)) || userArg->fname == NULL)
			{
				char* renameStr = (char*)malloc(sizeof(char) * LENGTH);
				strncpy(renameStr, search_path, strlen(search_path) + 1);
				renameStr = replacer(renameStr, userArg->searchStr, userArg->replaceStr);
				rename(search_path, renameStr);
				strncpy(search_path, renameStr, strlen(renameStr)+1);
				printf("this free");
				//free(renameStr);
			}
			//printf("first if");
		}
		else
		{
			//printf("first else");
			int status;
			char* myFile_path = (char*)malloc(sizeof(char) * LENGTH);
			if(path)
				snprintf(myFile_path, LENGTH, "%s/%s", search_path, meta->d_name);
			else
				snprintf(myFile_path, LENGTH, "%s/%s", myDir, meta->d_name);
			

			if((userArg->fname != NULL && strstr(myFile_path, userArg->fname)) || userArg->fname == NULL)
			{
				if((status = stat(myFile_path, curr_file_status))==0)
				{
					if((curr_file_status->st_mode & S_IRUSR) && S_ISREG(curr_file_status->st_mode))
					{
						//rename the file
						if(userArg->mode == 0)
						{
							printf("huzzah!\n");
							char* renameStr = (char*)malloc(sizeof(char) * LENGTH);
							strncpy(renameStr, myFile_path, strlen(myFile_path)+1);
							renameStr = replacer(renameStr, userArg->searchStr, userArg->replaceStr);
							rename(myFile_path, renameStr);
							printf("file %s\n", myFile_path);
							free(renameStr);
						}
						//search file and replace strings within file with new string
						else if(userArg->mode == 1)
						{
							mode1(myFile_path, userArg);
						}
					}
				} 
				else
				{
					fprintf(stderr, "Unknown Error \n");
				}
				free(myFile_path);
			}
		}
		closedir(search_path);
	}
	}
}

//if option is mode 1 this will be called
void mode1(char* myFile_path, struct userArgs* userArg)
{
	char* theBuff = (char*)malloc(sizeof(char)*LENGTH);
	char* strReplace = (char*)malloc(sizeof(char)*strlen(userArg->searchStr) + 1);
	strncpy(strReplace, userArg->searchStr, strlen(userArg->searchStr) +1);
	strncat(strReplace, userArg->replaceStr, strlen(userArg->replaceStr)+1);
	FILE* fileReplace = fopen(myFile_path, "r+");
	FILE* tempFile = tmpfile();
	if(fileReplace != NULL)
	{
		//searches file for word and replaces
		fseek(fileReplace, 0, SEEK_END);
		long fileSize = ftell(fileReplace);
		fseek(fileReplace, 0, SEEK_SET);
		char* checkFile = (char*)malloc(sizeof(char) * fileSize+1);
		fread(checkFile, fileSize, 1, fileReplace);

		if(strstr(checkFile, userArg->searchStr) == NULL)
		{
			rewind(fileReplace);
			while(fgets(theBuff, LENGTH, tempFile) != NULL)
			{
				fputs(theBuff, fileReplace);
			}
		}
		fclose(fileReplace);
	}
	else
	{
		fprintf(stderr, "Error Opening the file. \n");
	}
	fclose(tempFile);
	free(strReplace);
	free(theBuff);
}


int main(int argc, const char** argv)
{
	char* directory; 
	directory = (char*)malloc(sizeof(char) * LENGTH);
	struct userArgs* userArg = (struct userArgs*)malloc(sizeof(struct userArgs));
	int myOpt;
	/*getopt, thanks google for this
	 *parses the command-line arguments
	 *must be specified with -d -f -r -s -m respectively
	 */
	while((myOpt=getopt(argc, argv, "d:f:r:s:m:")) != -1){
		switch(myOpt){
			case 'd':
				//name of directory
				strncpy(directory, optarg, strlen(optarg) +1);
				break;
			case 'f':
				//Matching patttern to find
				userArg->searchStr = (char*)malloc(sizeof(char)*50);
				strncpy(userArg->searchStr, optarg, strlen(optarg)+1);
				break;
			case 'r':
				//String to replace pattern with
				userArg->replaceStr = (char*)malloc(sizeof(char)*50);
				strncpy(userArg->replaceStr, optarg, strlen(optarg)+1);
				break;
			case 's':
				//File and folder search
				userArg->fname = (char*)malloc(sizeof(char)*50);
				strncpy(userArg->fname, optarg, strlen(optarg)+1);
				break;
			case 'm':
				//Modes for either renaming (0) or scanning (1)
				userArg->mode = atoi(optarg);
				break;
			default:
				//Default if none of the above options work
				return EXIT_FAILURE;
		}
	}

	//char* myTest = replacer("this is my string", "string", "book");
	//printf("myTest %s", myTest);
	if(directory == NULL){
		directory="./";
	}

	searchDirectories(directory, NULL, NULL, userArg);
	free(directory);
	free(userArg->searchStr);
	free(userArg->replaceStr);
	if(userArg->fname != NULL)
		free(userArg->fname);
	free(userArg);
	return EXIT_SUCCESS;
}

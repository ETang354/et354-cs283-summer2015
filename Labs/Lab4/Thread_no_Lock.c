#include <stdio.h>
#include <pthread.h>

typedef struct shared_data
{
	int *x; //Shared Value
	//pthread_mutex_t *lock; // shared lock value
	int id; // Unique ID
} shared_data;

//only 1 arg per pthread
void* routine(void* arg)
{
	int i = 0;
	shared_data* data = (shared_data*) arg;

	while(i < 1000)
	{
		(*(data->x))++;
		printf("hello from thread %d! My value is %d\n", data->id, *(data->x));
		i++;
	}

}

int main(void)
{
	shared_data data[100];
	int val = 0;
	int i;
	pthread_t t[100];


	for(i=0; i<100; i++)
	{
		data[i].id = i;
		data[i].x = &val;
	//	data[i].lock = &lock;

		pthread_create(&t[i], NULL, routine, &data[i]);
		pthread_join(t[i], NULL);
	}


}

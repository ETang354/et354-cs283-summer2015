#include<stdio.h>
#include<stdlib.h>
#include<time.h>

int *add(int *a, int value, int size);
int *remov(int *a, int index, int size); 
int get(int *a, int index);

int main(void)
{
	int *p;
	p=malloc(5*sizeof(int));
	int i;
	for(i=0; i<5; i++)
	{
		p[i]=i*2;
		printf("%d ", p[i]);
	}
	int size = 5;
	p = add(p, 1, size);
	size++;
	printf("added value %d \n", p[6]);
	p = remov(p, 1, size);
	size--;
	printf("removed a value\n");
	int j;
	for(j=0;j<5;j++)
	{
		printf("%d ", p[j]);
	}
	int k = get(p, 3);
	printf("\ngetting index 3 = %d", k);

	int h;

	clock_t start, end;
	double time_taken;
	start = clock();
	for(h=0; h<100000; h++)
	{
		p = add(p, h, size);
		size++;
	}
	end = clock();
	time_taken = ((double)(end-start))/CLOCKS_PER_SEC;
	printf("\nfirst time taken = %lf\n", time_taken);
	
	start=clock();
	for(h=0; h<200000; h++)
	{
		p=add(p, h, size);
		size++;
	}
	end=clock();
	time_taken = ((double)(end-start))/CLOCKS_PER_SEC;
	printf("\n double time taken = %lf\n", time_taken);

	free(p);

}

int *add(int *a, int value, int size)
{
	a=realloc(a, (size+1)*sizeof(int));
	a[size] = value;
	return a;
}

int *remov(int *a, int index, int size)
{
	int i;
	for(i=index; i<size-1; i++)
	{
		a[i]=a[i+1];
	}
	return a;
}

int get(int *a, int index)
{
	return *(a+index);
}

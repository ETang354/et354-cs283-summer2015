/*Required Headers*/

#include <sys/types.h>
#include <sys/socket.h>
#include <netdb.h>
#include <stdio.h>
#include <string.h>

void *thread_connect(void *socket_desc);

int main(int argc, char *argv[])
{

    char str[100];
    char msg[100];
	   int listen_fd, comm_fd;

    struct sockaddr_in servaddr;

    listen_fd = socket(AF_INET, SOCK_STREAM, 0);

    bzero( &servaddr, sizeof(servaddr));

    servaddr.sin_family = AF_INET;
    servaddr.sin_addr.s_addr = htons(INADDR_ANY);
    servaddr.sin_port = htons(5000);

    bind(listen_fd, (struct sockaddr *) &servaddr, sizeof(servaddr));

    listen(listen_fd, 10);


    comm_fd = accept(listen_fd, (struct sockaddr*) NULL, NULL);

    if(comm_fd)
    {
      bzero(msg, 100);
      puts("Connection Established");
      strcpy(msg,"You are connected to Eddie Tang's server\n");
      write(comm_fd, msg, strlen(msg)+1);
      puts("did it hit here?");
    }

    while(1)
    {

        bzero( str, 100);

        read(comm_fd,str,100);

        printf("Echoing back - %s",str);
        write(comm_fd, str, strlen(str)+1);

    }
}

void *thread_connect(void *socket_desc)
{

}

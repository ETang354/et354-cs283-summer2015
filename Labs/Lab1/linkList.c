#include<stdio.h>
#include<stdlib.h>


struct ListNode{
	int data;
	struct ListNode *next;
};

void sort(struct ListNode *a, int size);
void swap(struct ListNode *a, struct ListNode *b);

int main(void){
	
	struct ListNode *root;
	struct ListNode *tmp;

	root = NULL;

	int i;
	for (i=0; i<10; i++){
		tmp = (struct ListNode*)malloc(sizeof(struct ListNode));
		if(i%2 == 0)
		{
			tmp->data=i;
		}else{
			tmp->data=i*2;
		}
		tmp->next=root;
		root=tmp;
	}

	tmp = root;

	while(tmp)
	{
		printf("%d ", tmp->data);
		tmp = tmp->next;
	}
	sort(tmp, 10);
}

void sort(struct ListNode *a, int size)
{
	struct ListNode *tmp = a;
	
	while(tmp->next != NULL)
	{
		if(tmp->data > tmp->next->data)
		{
			swap(tmp, tmp->next);
			
		}
	}
}

void swap(struct ListNode *a, struct ListNode *b){
	a->next = b->next;
	b->next = a;
}

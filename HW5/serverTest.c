#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <sys/socket.h>
#include <arpa/inet.h>
#include <unistd.h>
#include <pthread.h>


void *multiConnect(void *);

int main(int argc , char *argv[])
{

    int socket_desc , client_sock , c;
    struct sockaddr_in server , client;
    int thread_count = 0;

    socket_desc = socket(AF_INET , SOCK_STREAM , 0);
    if (socket_desc == -1)
    {
        printf("Could not create socket");
    }
    puts("Socket created");

    server.sin_family = AF_INET;
    server.sin_addr.s_addr = INADDR_ANY;
    server.sin_port = htons( 6666 );

    if( bind(socket_desc,(struct sockaddr *)&server , sizeof(server)) < 0)
    {
        perror("ERROR: bind failed");
        return 1;
    }

    listen(socket_desc , 3);

    puts("Connect to me");
    c = sizeof(struct sockaddr_in);

	pthread_t thread_id;

    while( (client_sock = accept(socket_desc, (struct sockaddr *)&client, (socklen_t*)&c)) )
    {
      //break out if more than 2 connections are trying to get through
      if(thread_count < 2)
        {

        puts("Connection accepted");
        printf("client_sock = %d",client_sock);
        if( pthread_create( &thread_id , NULL ,  multiConnect , (void*) &client_sock) < 0)
        {
            perror("could not create thread");
            return 1;
        }
        thread_count++;
      }
    }

    if (client_sock < 0)
    {
        perror("accept failed");
        return 1;
    }

    return 0;
}

//This will connect each client as their own thread
void *multiConnect(void *socket_desc)
{
    int sock = *(int*)socket_desc;
    int readSize;
    char *message , client_message[2000];
    printf("Socket desc in thread = %d", sock);
//printf and flush to go from server -> client

    while( (readSize = recv(sock , client_message , 2000 , 0)) > 0 )
    {

		client_message[readSize] = '\0';
    printf("Echoing back - %s\n", client_message);
		//Send the message back to other client
    if(sock == 4)
        write(sock+1 , client_message , strlen(client_message));
    else
      write(sock-1, client_message, strlen(client_message));
		memset(client_message, 0, 2000);
    }


    if(readSize == -1)
    {
        perror("recv failed");
    }
    else if(readSize == 0)
    {
        puts("Client disconnected");
        fflush(stdout);
    }
    return 0;
}

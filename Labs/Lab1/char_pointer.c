#include <stdio.h>
#include <stdlib.h>

int main(void)
{
	char **p;
	
	p = malloc(10*sizeof(*p));
	int i;

	for(i=0; i<10; i++)
	{
		p[i]=(char*)malloc(15*sizeof(char));
	
		strncpy(p[i], "word of fifteen", 15);
		printf("%s\n", p[i]);
	}
	int j;
	for(j=0; j<10; ++j)
	{
		free(p[j]);
	}
	free(p);
}

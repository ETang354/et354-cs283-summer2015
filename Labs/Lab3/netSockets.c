#include <stdlib.h>
#include <stdio.h>
#include <netdb.h>
#include <arpa/inet.h>
#include <sys/socket.h>
#include <string.h>

#define PORT 80
#define USERAGENT "HTMLGET 1.1"
#define PAGE "/"
#define HOST "www.google.com"


char* convertHost(char* host);
char* build_get_query(char* host, char* page);
int create_tcp_socket();

//builds query
char* build_get_query(char* host, char* page)
{
	char* query;
	char* getpage = page;
	char* tpi = "GET /%s HTTP/1.1\r\nHost: %s\r\nConnection: close\r\n\r\n";
	if(getpage[0] == '/')
	{
		getpage = getpage+1;
		fprintf(stderr, "Removing leading \"/\", converting %s to %s\n", page, getpage);
	}
	query = (char*)malloc(strlen(host)+strlen(getpage)+strlen(USERAGENT)+strlen(tpi)-5);
	sprintf(query, tpi, getpage, host, USERAGENT);
	return query;

}
//Creates tcp socket
int create_tcp_socket()
{
	int rv;
	if((rv = socket(AF_INET, SOCK_STREAM, IPPROTO_TCP))<0)
	{
		perror("Can't create TCP Sockets");
		exit(1);
	}
	return rv;

}
//get's IP from host
char* convertHost(char* host)
{
	struct hostent *hent;
	int lengthIP = 15;
	char* ip = (char *)malloc(lengthIP+1);

	memset(ip, 0, lengthIP+1);

	if((hent = gethostbyname(host)) == NULL)
	{
		herror("Can't get IP");
		exit(1);
	}

	if(inet_ntop(AF_INET, (void *)hent->h_addr_list[0], ip, lengthIP) == NULL)
	{
		perror("can't find host");
		exit(1);
	}
	return ip;
}

int main(int argc, char** argv)
{
	struct sockaddr_in* remote;
	char *host, *page, *ip, *get;
	int sock, tmpres;
	char buf[BUFSIZ+1];

	if(argc > 3 || argc < 2)
	{

		printf("wrong amount of arguments: %d\n", argc);
		return EXIT_FAILURE;
	}

	host = argv[1];

	if(argc == 2)
	{
		page = "/";
	}
	else
	{
		page = argv[2];
	}
//httpresponse should be equal to http/1.1 200 ok\r\n*
	sock = create_tcp_socket();

	//page = PAGE;
	//host = HOST;
	ip = convertHost(host);

	remote = (struct sockaddr_in *)malloc(sizeof(struct sockaddr_in));

	remote->sin_family = AF_INET;

	tmpres = inet_pton(AF_INET, ip, (void *)(&(remote->sin_addr.s_addr)));

	remote->sin_port = htons(PORT);

	if(connect(sock, (struct sockaddr *)remote, sizeof(struct sockaddr)) < 0 )
	{
		perror("Cannot connect");
	}

	get = build_get_query(host, page);
	printf("Debugging get: %s\n", get);
	int sendFlag = 0;

	while(sendFlag < strlen(get)){
		tmpres = send(sock, get + sendFlag, strlen(get) - sendFlag, 0);
		printf("tmpres %d\n", tmpres);
		if(tmpres == -1)
		{
			perror("Can't send yo stuff");
			return EXIT_FAILURE;
		}

		sendFlag += tmpres;
	}
	printf("here?\n");
	printf("sendFlag = %d\n", sendFlag);
	memset(buf, 0, sizeof(buf));
	int start = 0;
	char* content;

	while((tmpres = recv(sock, buf, BUFSIZ, 0)) > 0)
	{
		if(start == 0)
		{
			content = strstr(buf, "\r\n\r\n");
			if(content != NULL)
			{
				start = 1;
				content += 4;
			}
		}
		else
		{
			content = buf;
		}

		if(start)
		{
			fprintf(stdout, content);
		}
		memset(buf, 0, tmpres);
	}

	if(tmpres < 0)
	{
		printf(stderr, "No data received");
	}

	free(get);
	free(remote);
	free(ip);
	close(sock);
//cleanup
	printf("\n\n");
	//fprintf(stderr, "IP is %s\n", ip);
	//while(Fgets(buf, BUFSIZ+1, stdin) != NULL)
	//{
//		printf("Echo: ");
//		Fputs(buf, stdout);
//		printf("Enter message: "); fflush(stdout);
//	}
	//printf("get_query is %s\n", get);
	return EXIT_SUCCESS;

}

#ifndef __DEENCRYPT__
#define __DEENCRYPT__

typedef struct RSA
{
	long a,b,c,d,e;
} RSA;

long coprime(long x);
long endecrypt(long msg, long key, long c);
long decrypt(long y, long d, long c);
long GCD(long a, long b);
long mod_inverse(long base, long m);
long modulo(long a, long b, long c);
long totient(long n);
int prime(long x);
long getPrime(long x);
long RSA_Algorithm(RSA secure);
void askUser(RSA *pub);

#endif

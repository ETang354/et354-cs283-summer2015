#include <ftw.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdint.h>

static int display_info(const char *fpath, const struct stat *sb,  int tflag, struct FTW *ftwbuf) {
	    printf("%s\n", fpath);
		    return 0;           /* To tell nftw() to continue */
}

// http://linux.die.net/man/3/ftw
 int main(int argc, char *argv[]) {
     int flags = 0;

        if (nftw("/", display_info, 20, flags) == -1) { // calls display_info for each file found, including its stat and path!
                perror("nftw");
                        exit(EXIT_FAILURE);
                            }
                                exit(EXIT_SUCCESS);
                                }


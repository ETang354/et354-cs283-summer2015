#include <stdio.h>
#include <stdlib.h>

void sort(int *a, int size);

int main(void)
{
	int array[] = {1, 5, 3, 4, 2};
	int *p = array;
	int size = 5;
	int i;
	printf("original list:\n");
	for(i=0; i<size; i++)
		printf("%d ", *(p+i));

	sort(p, size);
}
void sort(int *a, int size)
{
	printf("\nsorting \n");
	int swap = 0;
	int j =0;
	int tmp;

	while(swap == 0)
	{
		swap = 1;
		j++;
		int i;
		for(i =0 ; i<size - j; i++)
		{
			if(*(a+i) > *(a+i+1)){
				tmp = *(a+i);
				*(a+i)=*(a+i+1);
				*(a+i+1)=tmp;
				swap = 0;
				//printf("temp: %d\n", tmp);
				//printf("swapped %d with %d\n", *(a+i), *(a+i+1));
			}
		}
	}
	printf("done sorting\n");
	int k;
	
	for(k = 0; k<size; k++)
	{
		printf("%d ", *(a+k));
	}
	printf("\n");
}

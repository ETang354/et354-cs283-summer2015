/*
|-----------------------------------------------------
| 	HTTP Server - Lab 3
|-----------------------------------------------------
|	Authors: Eddie Tang, Patrick McHugh
|   Project: Lab 3 - part 2
|   Description: The server that receives a connection
| 				 from a client and responds depending
|				 on a given page
|
*/

#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <errno.h>
#include <string.h>
#include <sys/types.h>
#include <time.h>
#include <sys/stat.h>
#include <unistd.h>
#include <fcntl.h>

char response[] = "Eddie Tang";

int main(int argc, char *argv[])
{
    int listenfd = 0, connfd = 0, port;
    struct sockaddr_in serv_addr;
	char path[1025];
    char sendBuff[1025];
    time_t ticks;
	char* filename = "b.txt";
	port = atoi(argv[1]);
	int fd;
	char * readFile;
	//creates socket
    listenfd = socket(AF_INET, SOCK_STREAM, 0);
    memset(&serv_addr, '0', sizeof(serv_addr));
    memset(sendBuff, '0', sizeof(sendBuff));
	//get serv_addr data
    serv_addr.sin_family = AF_INET;
    serv_addr.sin_addr.s_addr = htonl(INADDR_ANY);
    serv_addr.sin_port = htons(port);
	//binds the data
    bind(listenfd, (struct sockaddr*)&serv_addr, sizeof(serv_addr));
	//listens for client
    listen(listenfd, 10);
	//prepares file to send when listen receives something
	strcpy(path, "./");
	strcat(path, filename);
    while(1)
    {
        connfd = accept(listenfd, (struct sockaddr*)NULL, NULL);
		fd = open(path, O_RDONLY, 0);
        if(fd == -1)
		{
			fprintf(response, "Failed to open file closing connection\n");
			printf("%s", response);
			return;
		}
		while(read(fd, &readFile, 1) != 0)
		{
			write(connfd, &readFile, 1);
		}

		//sprintf(response,"%d", fd);
		ticks = time(NULL);
        snprintf(sendBuff, sizeof(sendBuff), "%.24s\r\n", ctime(&ticks));
		close(fd);
        close(connfd);
        sleep(1);
     }
}

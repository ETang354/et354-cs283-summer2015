#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include "deencrypt.h"
/*
long coprime(long x);
long endecrypt(long msg, long key, long c);
long GCD(long a, long b);
long mod_inverse(long base, long m);
long modulo(long a, long b, long c);
long totient(long n);
int prime(long x);
*/

long coprime(long x)
{
	long randomVal = (x * 2.5) + 1;
	while(1)
	{
		//printf("randomVal = %d\n", randomVal);
		if(GCD(x, randomVal) == 1)
		{
			//printf("randomVal_returned = %d\n", randomVal);
			return floor(randomVal);
		}
		randomVal++;
	}
}

long endecrypt(long msg, long key, long c)
{
	modulo(msg, key, c);
}

long decrypt(long y, long d, long c)
{
	int rv;
	int dc;
	dc = d%c;
	rv = y^dc;
	return rv;
}

long GCD(long a, long b)
{
	if(a==0)
		return b;
	return GCD(b%a, a);
}

long mod_inverse(long base, long m)
{
	int i;
	long multiplied;
	//formula used is [base*(m-1)]%m for looped values m=0 to m=m-1
	printf("base = %d, m = %d\n", base, m);
	for(i=0; i<m ;i++)
	{
		multiplied = base*i;
		if(i == 59)
		printf("multiplied = %d, base = %d\n", multiplied, base);
		if(multiplied%m == 1)
			return i;
		multiplied = 0;
	}
	//return base^-1 %m;
	//If this part is met, no modular inverse was found
	//therefore something is wrong with coprime
	printf("No Modular inverse found!\n");
	return 0;
}

long modulo(long a, long b, long c)
{
	//pow function is not working....
	//int power = pow(a,b);
	int i;
	long power = 1;
	for(i=0; i<b; i++)
	{
		power = (power*a)%c;
	}

	return power;
}

long totient(long n)
{
	long tmp;
	long count = 0;
	int i;
	tmp = n;

	if(prime(n)==0)
		return n-1;

	for(i = 0; i<n; i++)
	{
		if(GCD(n,i)==1)
			count ++;
	}

	return count;
}

int prime(long x)
{
	int i;
	for(i = 2; i<x; i++)
	{
		if(x%i == 0)
		{
			return 1;
			break;
		}
	}
	return 0;
}

long getPrime(long x)
{

	int i;
	int count = 2;

	if(x==1)
	{
		return 2;
	}

	for(i=3; count<x; i=i+2)
	{
		if(prime(i)==0)
		{
			count++;
		}
	}
	return i;
}

void askUser(RSA *pub)
{
	long x,y;
	char val[10];
	long m;
	printf("Enter in value 1:\n");
	scanf("%s", &val);
	x = atoi(val);
	printf("Enter in value 2:\n");
	scanf("%s", &val);
	y = atoi(val);

	printf("%d\n",x );
	pub->a = getPrime(x);
	pub->b = getPrime(y);
	pub->c = pub->a * pub->b;

	m = (pub->a - 1)*(pub->b - 1);
	printf("m = %d\n", m);

	pub->e = coprime(pub->c);
	pub->d = mod_inverse(pub->e, m);
}

long RSA_Algorithm(struct RSA secure)
{
	return NULL;
}

/*int main(void)
{
	long w = GCD(25, 10);
	long randomCoPrime = coprime(100);
	long mod_iverse = mod_inverse(100, randomCoPrime);
	printf("co_prime = %d\n mod_inverse = %d\n", randomCoPrime, mod_iverse);
	long tot = totient(500);
	printf("totient: %d\n", tot);
	return 0;
}
*/

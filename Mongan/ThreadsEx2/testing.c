#include <stdio.h>
#include <pthread.h>

typedef struct shared_data 
{
	int *x; //Shared Value
	pthread_mutex_t *lock; // shared lock value
	int id; // Unique ID
} shared_data;



//only 1 arg per pthread
void* routine(void* arg)
{
	int i = 0;
	shared_data* data = (shared_data*) arg;
	
	while(i < 1000)
	{
		pthread_mutex_lock(data->lock);
		(*(data->x))++;
		printf("hello from thread %d! My value is %d\n", data->id, *(data->x));
		pthread_mutex_unlock(data->lock);
		i++;
	}
}

int main(void)
{
	shared_data data1, data2;
	int val = 0;

	pthread_t t1, t2;

	pthread_mutex_t lock;
	pthread_mutex_init(&lock, NULL);

	data1.id =0;
	data1.x = &val;
	data1.lock = &lock;

	data2.id = 1;
	data2.x = &val;
	data2.lock = &lock;

	pthread_create(&t1, NULL, routine, &data1);
	pthread_create(&t2, NULL, routine, &data2);

	pthread_join(t1, NULL);
	pthread_join(t2, NULL);

	pthread_mutex_destroy(&lock);

}




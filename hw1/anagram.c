#include <stdlib.h>
#include <stdio.h>
#include <string.h>


//Structs
struct hash { //creates structure that stores the key and words associated with such key
	int* key;
	char* word;
};
typedef struct hash Hash;


//Prototypes
void HashData(Hash* h, char* user);
void hashWord(Hash* table, int* key, char* word, int counter, char* user);
int getLetterValue(char c);
void createHash(int* const key, char* const word, Hash* ht);
void findAnagram(char* userWord, Hash* h, int count);
//Main
int main(int argc, char *argv[])
{
	Hash* h;
	char* user;
	h = (Hash*)malloc(sizeof(Hash));
	//printf("Enter word to find anagrams for: \n");
	//scanf("%s", &user);
	user = argv[1];
	//printf("%s", user);
	HashData(h, user);
	return 0;
}

//Functions/Methods

//Creates the Hash struct and adds elements to it
void createHash(int* const key, char* const word, Hash* ht)
{
	(*ht).key = key;
	(*ht).word = word;
}

//Reads the dictionary values and puts it in the Hash struct
void HashData(Hash* h, char* user)
{
	int* index;
	int counter = 0;
	index = malloc(sizeof(int));

	FILE* dict = fopen("/usr/share/dict/words", "r");
	if(dict == NULL) {
		return;
    }
	char word[128];
	while(fgets(word, sizeof(word), dict) != NULL) {
		index=(int*)realloc(index, (counter+1)*sizeof(int));
		h=(Hash*)realloc(h, (counter+1)*sizeof(Hash));
		hashWord(h, index, word, counter, user);
		counter++;
	}
}

//populates the hash table with each word
void hashWord(Hash* table, int* key, char* word, int counter, char* user)
{
	int value;
	int stringCount; 
	int totalVal;
	int i;
	Hash* temp = (Hash*)malloc(sizeof(Hash));

	stringCount = strlen(word);
	value = 0;
	totalVal = 0;
	
	for(i = 0; i < stringCount-1; i++)
	{
		totalVal += getLetterValue(*(word + i));
	}
	*(key+counter) = totalVal;
	createHash(key, word, temp);
	*table = *temp;
	findAnagram(user, table, counter);
}
//uses the user's word and finds anagrams using the Hash struct created previously
//prints out the results
void findAnagram(char* userWord, Hash* h, int count){
	int i;
	int j;
	int userStrLen;
	int userHashNum=0;
	int matchCount = 0;
	char* testString = h->word;
	userStrLen = strlen(userWord);
	for(i = 0; i<userStrLen; i++)
	{
		userHashNum += getLetterValue(*(userWord+i));
	}

	if(userStrLen == strlen(h->word)-1)
	{	if(userHashNum == h->key[count])
		{	for(i = 0; i<(strlen(h->word)-1); i++)
			{	for(j = 0; j<userStrLen; j++)
				{
					if(*(testString+i) == *(userWord+j))
					{	matchCount++;
						//printf("%d, %c, %c\n", matchCount, *(testString+i), *(userWord+j));
						break;
					}
				}
			}
		}
	}
	if(matchCount == userStrLen)
		printf("%s", h->word);
}
int getLetterValue(char c)
{
	char a[] = {'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j',
		'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v',
		'w', 'x', 'y', 'z'};
	int i;
	for(i = 0; i<26; i++)
	{
		if( ((int) c == (int) *(a+i)) || (((int) c - 32) == (int) *(a+i)))
		{
			return (i+1);
		}
	}
}


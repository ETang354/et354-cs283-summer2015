#include <stdio.h>
#include <pthread.h>

void *doit(void *w)
{
	int x = 0;
	int sum = 0;
	while (x<50000000)
	{
		sum = sum+x;
		x=x+1;
	}
	printf("%d\n", sum);

}

void *labeuf(void *foo)
{
	printf("Hello from thread %d \n", foo);
}


int main(void)
{
					//pthread_create takes in struct pthread, any attribute, function, params to function
	pthread_t t, t2; 

	pthread_create(&t, NULL, doit, NULL);
	pthread_create(&t2, NULL, doit, NULL);

	//pthread_create(&t, NULL, labeuf, 1);
	//pthread_create(&t2, NULL, labeuf, 2);

	pthread_join(t, NULL);
	pthread_join(t2, NULL);//waits for threads
	
}


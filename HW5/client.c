#include <sys/types.h>
#include <sys/socket.h>
#include <netdb.h>
#include <stdio.h>
#include <string.h>
#include "deencrypt.h"

int main(int argc,char **argv)
{
    RSA pub;
    askUser(&pub);
    //printf("PUBLIC: a=%d, b=%d, c=%d, d=%d, e=%d\n", pub.a, pub.b, pub.c,pub.d, pub.e);
    printf("public(%d,%d)\n", pub.e, pub.c);
    printf("private(%d,%d)\n", pub.d, pub.c);

    int sockfd,n;
    char sendline[50];
    char recvline[50];
    char encryptStr[10];
    int i;
    long temp;
    struct sockaddr_in servaddr;

    sockfd=socket(AF_INET,SOCK_STREAM,0);
    bzero(&servaddr,sizeof servaddr);

    servaddr.sin_family=AF_INET;
    servaddr.sin_port=htons(6666);

    inet_pton(AF_INET,"tux64-12.cs.drexel.edu",&(servaddr.sin_addr));

    connect(sockfd,(struct sockaddr *)&servaddr,sizeof(servaddr));

    while(1)
    {
        bzero( sendline, 50);
        bzero( recvline, 50);
        bzero(encryptStr, 50);
        fgets(sendline,50,stdin); /*stdin = 0 , for standard input */

        for(i=0; i<sizeof(sendline); i++)
        {
          if(sendline[i] == '\0')
          {
            break;
          }
          temp = endecrypt(sendline[i], pub.e, pub.c);
          sprintf(encryptStr, "%d", temp);
          write(sockfd,encryptStr,strlen(sendline)+1);
        }
        //write(sockfd,encryptStr,strlen(sendline)+1);
        read(sockfd,recvline,50);
        printf("%s",recvline);
    }

}
